<!doctype html>
<?php
//Processing des données entrées dans le formulaire (pour accepter ou non)
include ('database.php');
session_start();

//Checker si déjà connecté
if($_SESSION['logged']==true)
{
	//Se déconnecter
	$_SESSION['logged']=false;
}

//message d'erreur vide au début
$error = "";	

//Mettre le 'mail' à vide au départ
$sujet = "";
$entete = "";
$sendmail = "";

//Check si demande d'inscription
if(isset($_GET['forgot']))
{	 
	//Checker les mot de passe
	if($_GET['email'] != $_GET['confirmation'])
	{
		$error = "Les emails ne sont pas similaires, veuillez réessayer !";
		//header("Location: formulaire_entree.php");
	}
	else
	{
		//Récupérer les trucs de GET
		$email = $_GET['email'];
		
		//vérif et connection à la database
		$sql = "SELECT email FROM members WHERE email = '$email' ";
		
		if (mysqli_query($conn, $sql)) 
		{
			$error = "Un mail de modification vous a été envoyé ! :D";
			echo "succès de l'opération !";
			// Préparation du mail contenant le lien d'activation
			$destinataire = $email;
			$sujet = "Sujet : Mot de Passe oublié" ;
			$entete = "From : mdp@gentilsite.com" ;
 
			// Le lien de modification avec pseudo dans les get
			$sendmail = 'Nous sommes le Gentil Site,</br>
 
			Pour modifier votre mot de passe, veuillez cliquer sur le lien ci-dessous</br>
			ou copier/coller dans votre navigateur Internet : </br></br>
 
			http://127.0.0.1/edsa-IR_projet/change_mdp.php?mailactuel='.urlencode($email).'</br></br>
 
 
			---------------</br>
			Ceci est un mail automatique, Merci de ne pas y répondre.</br>';
		} 
		else
		{
			$error = "Erreur SQL member creation: " . $sql . "" . mysqli_error($conn);
		}
		mysqli_close($conn);
	}
}
?>
<html lang="fr">
<head>
	  <meta charset="utf-8">
      <title>Page mot de passe oublié</title>
      
      <style type = "text/css">
         body {
            font-family:Arial, Helvetica, sans-serif;
            font-size:14px;
         }
         label {
            font-weight:bold;
            width:100px;
            font-size:14px;
         }
         .box {
            border:#666666 solid 1px;
         }
      </style>
      <h1 align='center'>Reset de mot de passe ! :D</h1>
</head>
<body bgcolor = "#FFFFFF">
	
      <div align = "center">
		 <h2>Mot de passe oublié</h2>
         <div style = "width:300px; border: solid 1px #333333; " align = "left">
            <div style = "background-color:#333333; color:#FFFFFF; padding:3px;"><b>Indiquez votre email</b></div>
				
            <div style = "margin:30px">
				<form method="get" action="">
				Email Récupération:<br>
				<input type="email" name="email">
				<br><br>
				Confirmer Email:<br>
				<input type="email" name="confirmation">
				<br><br>
				<input type="submit" name="forgot" value="Submit">
				</form>
			
			<div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
			
			</div>
				
         </div>
			
      <div style = "font-size:14px; color:#1CC1FF; margin-top:10px"><?php echo $sujet ."</br>". $entete ."</br>". $sendmail; ?></div>

</body>
</html>