<?php
   //Processing du Log In
   include("database.php");
   session_start();
   
    $error = "";	//message d'erreur vide au départ
	
    //if($_SERVER["REQUEST_METHOD"] == "GET") {
	if(isset($_GET['log'])) {
      // username and password sent from form 
      
      $pseudo = mysqli_real_escape_string($conn,$_GET['pseudo']);
      $password = mysqli_real_escape_string($conn,$_GET['mdp']); 
      
      $sql = "SELECT pseudo FROM sso WHERE pseudo = '$pseudo' and mdp = '$password'";
      $result = mysqli_query($conn,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $active = $row['active'];
      
      $count = mysqli_num_rows($result);
      
      // If result matched $pseudo and $password, table row must be 1 row
		
      if($count == 1) {
         $_SESSION['logged'] = true;
         $_SESSION['pseudo'] = $pseudo;
		 $_SESSION['SSO']=true;
         header("location: gentil_site_main.php");
      }
	  else 
	  {
         $error = "Pseudo ou Mot de Passe SSO invalide !";
      }
	  mysqli_close($conn);
   }
?>
<html>
   
   <head>
	  <meta charset="utf-8">
      <title>Page de Login SSO</title>
      
      <style type = "text/css">
         body {
            font-family:Arial, Helvetica, sans-serif;
            font-size:14px;
         }
         label {
            font-weight:bold;
            width:100px;
            font-size:14px;
         }
         .box {
            border:#666666 solid 1px;
         }
      </style>
      <h1 align='center'>Loggez-vous à travers un site tiers (SSO) ! :D</h1>
   </head>
   
   <body bgcolor = "#FFFFFF">
	
      <div align = "center">
	     <h2>Login SSO</h2>
         <div style = "width:300px; border: solid 1px #333333; " align = "left">
            <div style = "background-color:#333333; color:#FFFFFF; padding:3px;"><b>Login</b></div>
				
            <div style = "margin:30px">
               
               <form action = "" method = "get">
                  <label>Pseudo  :</label><br/><input type = "text" name = "pseudo" class = "box"/><br /><br />
                  <label>Mot de Passe  :<br/></label><input type = "password" name = "mdp" class = "box" /><br /><br />
                  <input type = "submit" name="log" value = "Submit"/><br />
               </form>
               
               <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
					
            </div>
				
         </div>
			
      </div>

   </body>
</html>