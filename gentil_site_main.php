<!doctype html>
<?php
   include ('session.php');	//inclure la session
?>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Gentil Site</title>
<h1 align='center' style = "color:#68FF1C"> Bienvenue sur notre Gentil Site plein de fleurs ! </h1>
<p>Connecté en tant que : 
 <?php 
  if($_SESSION['logged']==true)
  {
		echo $_SESSION['pseudo'];
		if(isset($_SESSION['pseudoSSO']))
			echo '   -   '.$_SESSION['pseudoSSO'];
		echo '</br><a href="logout.php"><span>Logout</span></a></li>';
		if($_SESSION['SSO']==false)
		{
			echo "</br><a href='change_email.php'>Changement d'email</a>";
			echo '</br><a href="SSO_link.php">Lier son compte SSO</a>';
		}
			
  }
  elseif($_SESSION['logged']==false)
  {
		echo 'Anonymous ';
		echo '</br><a href="formulaire_login.php"><span>Login</span></a></li>';
		echo '</br><a href="formulaire_entree.php">Inscription</a>';
		echo '</br><a href="formulaire_login_vip.php"><span>Login VIP</span></a></li>';
		echo '</br><a href="formulaire_entree_vip.php">Inscription VIP</a>';
		echo '</br><a href="SSO_login.php">Login SSO</a>';
		echo '</br><a href="SSO_login_Post.php">Login SSO avec POST</a>';
  }
 ?>
</p>
</head>
<body>
	<div align = "center">
	</br>
	<p>
		Le gentil site est un site gentil qui vous propose un panel de fleurs à regarder pour vous rendre heureux ! :)
	</br>
	<img src="https://www.sciencesetavenir.fr/assets/img/2020/07/09/cover-r4x3w1000-5f774742ea5ce-flowers-19830-1920.jpg"
				alt="Fleur 1 !" width="400" border="0" align="center">
			
	<img src="https://emova-monceaufleurs-fr-storage.omn.proximis.com/Imagestorage/images/2560/1600/5f6dbfaf6c522_visuel2_1_.jpg"
				alt="Fleur 2 !" width="400" border="0" align="center">
	</br>			
	<img src="https://www.herboristerie-moderne.fr/mbFiles/images/plantes/photos/thumbs/800x600/mauve-sylvestre-herboristerie-moderne.jpg"
				alt="Fleur 3 !" width="400" border="0" align="center">
			
	<img src="https://www.semaille.com/8359/zinnia-a-grandes-fleurs.jpg"
				alt="Fleur 4 !" width="400" border="0" align="center">
	</br>
	</p>
	<?php
		if($_SESSION['logged']==true)
		{
			echo '<p> Comme vous êtes connecté, vous méritez plus de fleurs ! ;) </br>';
			echo '<img src="https://blog.interflora.fr/wp-content/uploads/2014/04/Fleurs-et-couleurs-1280x720.jpg"
				  alt="Fleur 1 !" width="400" border="0" align="center">';
			echo '<img src="https://cdn.pixabay.com/photo/2019/07/08/07/24/bee-4324143_960_720.jpg"
				  alt="Fleur 1 !" width="400" border="0" align="center"></br>';
			echo '<img src="https://grainesdemane.fr/wp-content/uploads/2019/05/tournesol-couverture-768x472.jpg"
				  alt="Fleur 1 !" width="400" border="0" align="center">';
			echo '<img src="https://img.freepik.com/photos-gratuite/gros-plan-belle-fleur-daisy-oxeye_181624-11106.jpg?size=626&ext=jpg"
				  alt="Fleur 1 !" width="400" border="0" align="center"></br>';
			echo '</p>';
		}
	?>
	</div>
	<?php
		echo "Date actuelle : ";
		echo date("l F d, Y");
	?>
</body>
</html>